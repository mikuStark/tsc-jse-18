package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllTaskByProjectIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "find-all-task-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find all task by project id";
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectToTaskService().findTaskByProjectId(projectId);
        for (Task task : tasks) {
            System.out.println(task.toString());
        }
    }
}
