package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "find-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(id);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }
}
