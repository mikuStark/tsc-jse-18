package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "remove-by-id-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by id task";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeById(id);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getProjectToTaskService().removeById(id);
    }
}
