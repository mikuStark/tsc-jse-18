package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class TaskStartByNameCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "status-start-by-name-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start by name";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startByName(name);
        if (task == null) throw new TaskNotFoundException();
    }
}
