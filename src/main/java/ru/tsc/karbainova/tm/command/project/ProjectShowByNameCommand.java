package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "find-by-name-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find by name project";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }
}
