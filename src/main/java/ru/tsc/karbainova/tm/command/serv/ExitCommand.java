package ru.tsc.karbainova.tm.command.serv;

import ru.tsc.karbainova.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {
    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Exit app";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
