package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {
    void add(Project project);

    void remove(Project project);

    boolean existsById(String id);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void clear();

    Project findById(String id);

    Project findByIndex(int index);

    Project findByName(String name);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);


}
